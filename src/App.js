import React, { Component } from 'react';
import Widgets from './components/Widgets';
import Login from './components/Login';
import Signup from './components/Signup';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      loginScreen: true,
      signupScreen: false,
      widgetScreen: false,
      widgets: null
    }
  }

  handleScreens = ({loginScreen, signupScreen,widgetScreen, widgets}) => {
    if(widgetScreen != null && widgets != null) {
      this.setState({
        loginScreen,
        signupScreen,
        widgetScreen,
        widgets
      })
    }
    else{
      this.setState({
        loginScreen,
        signupScreen
      })
    }
  }

  // state = {
  //   widgets: [
  //     {
  //       id: 1,
  //       name: "A Hidden Widget",
  //       description: "Widget 1",
  //       kind: "hidden"
  //     },
  //     {
  //       id: 2,
  //       name: "2nd Public Widget",
  //       description: "Widget 2",
  //       kind: "visible"
  //     },
  //     {
  //       id: 3,
  //       name: "Last visible Widget",
  //       description: "Widget 3",
  //       kind: "visible"
  //     }
  //   ]
  // }
  render() {
    const {
      state: {
        loginScreen,
        widgets,
        widgetScreen,
        signupScreen
      }
    } = this;
    return (
      <div className="App">
        <div className="App-header">
          {loginScreen && <Login handleScreens={this.handleScreens} />}
          {widgets && widgetScreen && <Widgets widgets={widgets} handleScreens={this.handleScreens} /> }
          {signupScreen && <Signup handleScreens={this.handleScreens} />}
        </div>
      </div>
    );
  }
}

export default App;
