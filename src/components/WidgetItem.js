import React, { Component } from 'react';
import '../App.css';
import PropTypes from 'prop-types'

class WidgetItem extends Component {

  widgetStyle = () => {
    return {
      backgroundColor: '#f4f4f4',
      padding: '10px',
      borderBottom: '1px #ccc dotted',
      textDecoration: this.props.widget.kind === 'visible' ? 'none' : 'line-through'
    }
  }

  render() {
    return (
      <div style={this.widgetStyle()}>
        <h3>{this.props.widget.name}</h3>
        <p>{this.props.widget.description}</p>
      </div>
    )
  }
}

WidgetItem.propTypes = {
  widget: PropTypes.object.isRequired
}

export default WidgetItem;
