import React, { Component } from 'react';
import Axios from 'axios';

class Signup extends Component {
  constructor(props){
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      password: "",
      email: ""
    }
  }
  handleTextInputChange = (event) => {
    this.setState({
        [event.target.name]: event.target.value
    })
  }

  handleSignup = async () => {
    const {
      state: {
        firstName,
        lastName,
        password,
        email
      },
      props: {
        handleScreens
      }
    } = this
    const signUpObject = {
      client_id: "277ef29692f9a70d511415dc60592daf4cf2c6f6552d3e1b769924b2f2e2e6fe",
      client_secret: "d6106f26e8ff5b749a606a1fba557f44eb3dca8f48596847770beb9b643ea352",
      user: {
        first_name: firstName,
        last_name: lastName,
        password: password,
        email: email,
        image_url: "https://static.thenounproject.com/png/961-200.png"
      }
    }
    const response = await Axios.post('https://obscure-coast-28260.herokuapp.com/api/user', signUpObject);
    Axios.defaults.headers.common['Authorization'] = `Bearer ${response.data.token.access_token}`
    const widgetResponse = await Axios.get('https://obscure-coast-28260.herokuapp.com/api/user/my_widgets?client_id=277ef29692f9a70d511415dc60592daf4cf2c6f6552d3e1b769924b2f2e2e6fe&client_secret=d6106f26e8ff5b749a606a1fba557f44eb3dca8f48596847770beb9b643ea352&term=visible')
    handleScreens({
        signupScreen :false,
        widgetScreen : true,
        widgets: widgetResponse.data.widgets
    })
    console.log('response', response)
  }

  render(){
    const {
      state: {
        firstName,
        lastName,
        password,
        email
      },
      props: {
        handleScreens
      },
      handleTextInputChange,
      handleSignup
    } = this;
    return(
      <div>
        <button onClick={()=> {handleScreens({loginScreen: true, signupScreen: false})}}>Go To Login</button>
        <div>
          <label>First Name :</label>
          <input name="firstName" value={firstName} type="text" onChange={handleTextInputChange} />
        </div>
        <div>
          <label>Last Name :</label>
          <input name="lastName" value={lastName} type="text" onChange={handleTextInputChange} />
        </div>
        <div>
          <label>Email :</label>
          <input name="email" value={email} type="text" onChange={handleTextInputChange} />
        </div>
        <div>
          <label>Password :</label>
          <input name="password" value={password} type="password" onChange={handleTextInputChange} />
        </div>
        <button onClick={handleSignup}>
          Signup
        </button>
      </div>
    )
  }
}

export default Signup;