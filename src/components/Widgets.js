import React, { Component } from 'react';
import '../App.css';
import WidgetItem from './WidgetItem';

class Widgets extends Component {
  render() {
    const {
      props: {
        widgets
      }
    } = this
    return (
      widgets.map((widget) => (
        <WidgetItem key={widget.id} widget={widget}/>
      ))
    );
  }
}

export default Widgets;
