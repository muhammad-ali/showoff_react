import React, { Component } from 'react';
import Axios from 'axios';

Axios.defaults.headers.post['Content-Type'] = 'application/json'

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
        }
    }

    handleTextInputChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    handleLoginRequest = async () => {
        const {
            state: {
                email,
                password
            },
            props: {
                handleScreens
            }
        } = this;
        const loginRequest = {
            grant_type: "password",
            client_id: "277ef29692f9a70d511415dc60592daf4cf2c6f6552d3e1b769924b2f2e2e6fe",
            client_secret: "d6106f26e8ff5b749a606a1fba557f44eb3dca8f48596847770beb9b643ea352",
            username: email,
            password: password
        }
        const response = await Axios.post('https://obscure-coast-28260.herokuapp.com/api/authenticate', loginRequest);
        Axios.defaults.headers.common['Authorization'] = `Bearer ${response.data.token.access_token}`
        const widgetResponse = await Axios.get('https://obscure-coast-28260.herokuapp.com/api/user/my_widgets?client_id=277ef29692f9a70d511415dc60592daf4cf2c6f6552d3e1b769924b2f2e2e6fe&client_secret=d6106f26e8ff5b749a606a1fba557f44eb3dca8f48596847770beb9b643ea352&term=visible')
        handleScreens({
            loginScreen :false,
            signupScreen: false,
            widgetScreen : true,
            widgets: widgetResponse.data.widgets
        })
        console.log('response', response)
    }

    render() {
        const {
            state: {
                email,
                password
            },
            props: {
                handleScreens
            },
            handleTextInputChange,
            handleLoginRequest
        } = this;
        return (
            <div>
                <button onClick={()=> {handleScreens({loginScreen: false, signupScreen: true})}}>Go to Sign Up</button>
                <div>
                    <label>Email :</label>
                    <input name="email" value={email} type="text" onChange={handleTextInputChange} />
                </div>
                <div>
                    <label>Password :</label>
                    <input name="password" value={password} type="password" onChange={handleTextInputChange} />
                </div>
                <button onClick={handleLoginRequest}>Login</button>
            </div>
        )
    }

}

export default Login;
